const express = require("express");
const app = express();
const hbs = require('hbs');

const port = process.env.PORT || 3000

app.use(express.static(__dirname + '/public'))


// Express HBS engine
hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs');

app.get("/", function (req, res) {
    res.render('home', {

    })
});

app.get("/about", function (req, res) {
    res.render('about', {

    })
});
app.get("/gallery", function (req, res) {
    res.render('gallery', {

    })
});

app.listen(port, () => console.log(`escuchando peticiones en el puerto ${port}`));